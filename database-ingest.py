import random
from nltk import sent_tokenize
from nltk import punkt
from textblob import TextBlob
import sqlite3


#https://www.government.se/speeches/
#https://www.presidentti.fi/en/speeches/

#BOOK-KEEPING:
#Sauli: all of 2019, all of 2020, all of 2021
#Löfven: all of 2020, all of 2019, all of 2018


#ERRORS: partly problm with https://www.government.se/speeches/2018/07/anforande-for-statsminister-lofven-caac-9-juli/
#ingested until sentence: "Yet, we are not doing nearly enough to protect our children."

#ERRORS: You have shown us the meaning of duty, sacrifice and courage.
#ingested until sentence: https://www.government.se/speeches/2018/04/prime-minister-stefan-lofvens-speech-on-the-anniversary-of-the-terrorist-attack-on-drottninggatan/


### SET OPTIONS AND PARAMETERS

insert_into_database = None

Question = input("\nInsert into database? (y) ")

if Question.lower() == ("y"):
	insert_into_database = True
else:
	insert_into_database = False


#quotes table fields:
quote_id = None
quote_id_list = []
sentence = ""
speaker = "L"
polarity = None
subjectivity = None
length = None
sentence_type = None
language = "en"
#speech_id

#speeches table fields
speech_id = None

date = "2018-02-14"

title = "Speech on the 2030 Agenda for Children: End Violence Solutions Summit"

url = "https://www.government.se/speeches/2018/02/speech-on-the-2030-agenda-for-children-end-violence-solutions-summit/"

###CHECK WHETHER SPEECH ALREADY EXISTS IN DATABASE

conn = sqlite3.connect('speeches.db')
cur = conn.cursor()

li_sql = """SELECT * FROM speeches WHERE url='""" + url + """';"""
test = cur.execute(li_sql)

if test.fetchone() != None:	
	print("\n")
	print('Speech already exists.')
	print("\n")
	exit()


### GET LAST ROW ID FOR SPEECHES TABLE


li_sql = """SELECT * FROM speeches ORDER BY speech_id DESC LIMIT 1;"""
speeches_lastrow = cur.execute(li_sql)

speech_id = speeches_lastrow.fetchone()[0] + 1 #increment by one



### GET LAST ROW ID FOR QUOTES TABLE

li_sql = """SELECT * FROM quotes ORDER BY quote_id DESC LIMIT 1;"""
speeches_lastrow = cur.execute(li_sql)

quote_id = speeches_lastrow.fetchone()[0] + 1 #increment by one

quote_id_list.append(quote_id)

cur.close()
conn.close()




### WRITE ONCE TO SPEECHES TABLE

if insert_into_database is True:

	conn = sqlite3.connect('speeches.db')
	cur = conn.cursor()

	li_sql = """INSERT INTO speeches(speech_id, date, title, url) VALUES(?, ?, ?, ?);"""

	cur.execute(li_sql, (speech_id, date, title, url))
	conn.commit()

	cur.close()
	conn.close()




### SPLIT INTO SENTENCES AND ADD TO QUOTES TABLE IN LOOP

file = open("raw-speech.txt")
sentences = file.read()
file.close()

sorted_list = list(sorted(sent_tokenize(sentences), key = len))

sorted_list_cleaned = [s.replace('*', '').replace('\n', '').replace('\"', '').strip() for s in sorted_list]



for x in sorted_list_cleaned:
	sentence = x
	length = len(x)

	sentiment = TextBlob(x)

	polarity = round(sentiment.sentiment.polarity, 2)
	subjectivity = round(sentiment.sentiment.subjectivity, 2)

	if x.endswith("?"):
		sentence_type = "question"

	elif x.endswith("!"):
		sentence_type = "exclamation"
	else:
		sentence_type = "ordinary"

	li_sql = """INSERT INTO quotes(quote_id, sentence, speaker, polarity, subjectivity, length, sentence_type, language, speech_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);"""
	

	if insert_into_database is True:

		conn = sqlite3.connect('speeches.db')
		cur = conn.cursor()

		cur.execute(li_sql, (quote_id, sentence, speaker, polarity, subjectivity, length, sentence_type, language, speech_id))
		conn.commit()

		cur.close()
		conn.close()



	else:

		print("\n")
		print("quote_id: {}".format(quote_id))
		print("sentence: {}".format(sentence))
		print("speaker: {}".format(speaker))
		print("polarity: {}".format(polarity))
		print("subjectivity: {}".format(subjectivity))
		print("length: {}".format(length))
		print("sentence_type: {}".format(sentence_type))
		print("language: {}".format(language))
		print("speech_id: {}".format(speech_id))


	quote_id = quote_id + 1

	quote_id_list.append(quote_id)

print("All quote_id in list: {}".format(quote_id_list)) #sista id överlopps dvs inte riktigt id


if insert_into_database is True:


	import random
	from nltk import sent_tokenize
	from nltk import punkt
	from textblob import TextBlob
	from textblob import Word
	import sqlite3
	import time

	conn = sqlite3.connect('speeches.db')
	conn.row_factory = sqlite3.Row #to be able to access results like dictionary
	cur = conn.cursor()


	for y in range(quote_id_list[0], quote_id_list[-1]):

		li_sql = """SELECT * FROM quotes WHERE quote_id ={};""".format(y)
		sql_run = cur.execute(li_sql)
		result1 = sql_run.fetchone()

		blob_sentence = TextBlob(result1["sentence"])
		blob_tags = blob_sentence.tags

		result_list = []
		verb_list =[]
		noun_list = []
		adjective_list = []
		relevant_list = []
		word_id = []

		print("\n{}\n".format(result1["sentence"]))

		for x in blob_tags:
			w = Word(x[0])
			w_lem = w.lemmatize("v")

			result_list.append([w_lem.lower(), x[0], x[1]])

			if x[1].startswith("V") and len(x[0]) > 1:
				verb_list.append(w_lem.lower())

			if x[1].startswith("N") and len(x[0]) > 1:
				noun_list.append(w_lem.lower())

			if x[1].startswith("J") and len(x[0]) > 1:
				adjective_list.append(w_lem.lower())

		relevant_list = verb_list + noun_list + adjective_list

		#delete irrelevant words
		while "be" in relevant_list: relevant_list.remove("be")    
		while "have" in relevant_list: relevant_list.remove("have")
		while "do" in relevant_list: relevant_list.remove("do")
		while "does" in relevant_list: relevant_list.remove("does")
		while "wasn" in relevant_list: relevant_list.remove("wasn")
		while "isn" in relevant_list: relevant_list.remove("isn")
		while "'ve" in relevant_list: relevant_list.remove("'ve")
		while "'m" in relevant_list: relevant_list.remove("'m")		

		relevant_list = list(dict.fromkeys(relevant_list)) #remove duplicates
		relevant_list_len = len(relevant_list)

		print("\n")
		print("Verbs:          {}".format(verb_list))
		print("Adj:            {}".format(adjective_list))
		print("Nouns           {}".format(noun_list))
		print("Relevant words: {}".format(relevant_list))
		print("\n")

		#time.sleep(0.5)

		#print("{} {}".format(relevant_list_len, y))


		###ADD TO QUOTES TABLE
		#add number of column relevant_words_count

		li_sql = """UPDATE quotes SET relevant_words_count = ? WHERE quote_id = ?;"""

		cur.execute(li_sql, (relevant_list_len, y))
		conn.commit()

		print("Written to quotes table.")


		###ADD TO WORD TABLE
		###FOR LOOP THROUGH WORD LIST

		for z in relevant_list:

			#check if word exists in word table
			li_sql = """SELECT * FROM words WHERE word='{}';""".format(z)
			test = cur.execute(li_sql)

			if test.fetchone() == None: #doesn't exist = create new line in word table

				#get id for new line
				li_sql = """SELECT * FROM words ORDER BY word_id DESC LIMIT 1;"""
				lastrow = cur.execute(li_sql)
				get_id = lastrow.fetchone() 

				if get_id == None: #if table empty
					word_id = 1
				else:
					word_id = get_id["word_id"] + 1

				#insert word on new line
				li_sql = """INSERT INTO words(word_id, word) VALUES(?, ?);"""			
				cur.execute(li_sql, (word_id, z))
				conn.commit()

			#exists = get word_id
			li_sql = """SELECT * FROM words WHERE word='{}';""".format(z)
			test = cur.execute(li_sql)
			result = test.fetchone()
			word_id = result["word_id"]

			print("Written to words table.")


			###ADD TO CROSS REF TABLE
			li_sql = """INSERT INTO cross_ref(quote_id, word_id) VALUES(?, ?);"""			
			cur.execute(li_sql, (str(y), str(word_id)))
			conn.commit()

			print("Written to cross_ref table.")



	cur.close()
	conn.close()






