
import shutil
from PIL import Image, ImageDraw, ImageFont, ImageTk
import random

template_folder = "templates_2021-01-23/"
pearl_folder = "unedited_pearls/"
fixed_pearl_folder = "edited_pearls/"



strip_bases = ["strip_2021-02-01_08.33.19", "strip_2021-02-01_18.12.34", "strip_2021-02-01_18.31.08", "strip_2021-02-01_18.32.37", "strip_2021-02-01_18.58.16", "strip_2021-02-01_19.01.00", "strip_2021-02-01_22.05.39", "strip_2021-02-01_22.10.41", "strip_2021-02-02_19.19.57", "strip_2021-02-02_19.43.10", "strip_2021-02-03_08.45.00", "strip_2021-02-03_08.45.42", "strip_2021-02-03_08.46.09", "strip_2021-02-03_08.46.35", "strip_2021-02-05_22.06.04", "strip_2021-02-05_22.07.05", "strip_2021-02-05_22.07.24", "strip_2021-02-05_22.09.25", "strip_2021-02-05_22.10.09", "strip_2021-02-07_14.31.15", "strip_2021-02-07_14.39.48", "strip_2021-02-07_14.50.13"]


for strip_base in strip_bases:

	number1 = "Numbering_square1_v{}.png".format(random.randint(1,2))
	number2 = "Numbering_square2_v{}.png".format(random.randint(1,2))
	number3 = "Numbering_square3_v{}.png".format(random.randint(1,2))


	#öppna 1 2 3
	for x in [[1, number1], [2, number2], [3, number3]]:

		background_file = "{}{}_{}.png".format(pearl_folder, strip_base, x[0])
		background = Image.open(background_file, "r")

		pic_numbering_file = "{}{}".format(template_folder, x[1])
		pic_title = Image.open(pic_numbering_file, "r")
		background.paste(pic_title, (0,0), mask=pic_title)

		img_filename = "{}{}_{}.png".format(fixed_pearl_folder, strip_base, x[0])

		background.save(img_filename, format="PNG")


	#öppna full

	background_file = "{}{}_full.png".format(pearl_folder, strip_base)
	background = Image.open(background_file, "r")

	pic_numbering_file = "{}{}".format(template_folder, number1)
	pic_title = Image.open(pic_numbering_file, "r")
	background.paste(pic_title, (0,0), mask=pic_title)

	pic_numbering_file = "{}{}".format(template_folder, number2)
	pic_title = Image.open(pic_numbering_file, "r")
	background.paste(pic_title, (1080,0), mask=pic_title)

	pic_numbering_file = "{}{}".format(template_folder, number3)
	pic_title = Image.open(pic_numbering_file, "r")
	background.paste(pic_title, (2160,0), mask=pic_title)


	img_filename = "{}{}_full.png".format(fixed_pearl_folder, strip_base)

	background.save(img_filename, format="PNG")


	#kopiera uppdateringstexten rakt av

	copy_from_path = "{}{}_update.txt".format(pearl_folder, strip_base)
	copy_to_path = "{}{}_update.txt".format(fixed_pearl_folder, strip_base)

	shutil.copy(copy_from_path, copy_to_path)

