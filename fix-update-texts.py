
import re
import random

#öppna fil som slutar på _update.txt

strip_bases = ["strip_2021-02-01_08.33.19"] 

'''
"strip_2021-02-01_18.12.34", 
"strip_2021-02-01_18.31.08", 
"strip_2021-02-01_18.32.37", 
"strip_2021-02-01_18.58.16", 
"strip_2021-02-01_19.01.00", 
"strip_2021-02-01_22.05.39", 
"strip_2021-02-01_22.10.41", 
"strip_2021-02-02_19.19.57", 
"strip_2021-02-02_19.43.10", 
"strip_2021-02-03_08.45.00", 
"strip_2021-02-03_08.45.42", 
"strip_2021-02-03_08.46.09", 
"strip_2021-02-03_08.46.35", 
"strip_2021-02-05_22.06.04", 
"strip_2021-02-05_22.07.05", 
"strip_2021-02-05_22.07.24", 
"strip_2021-02-05_22.09.25", 
"strip_2021-02-05_22.10.09", 
"strip_2021-02-07_14.31.15", 
"strip_2021-02-07_14.39.48", 
"strip_2021-02-07_14.50.13", 
"strip_2021-02-07_19.02.41", 
"strip_2021-02-08_22.35.56", 
"strip_2021-02-09_21.58.11", 
"strip_2021-02-09_22.26.27", 
"strip_2021-02-09_22.38.32", 
"strip_2021-02-10_12.06.47", 
"strip_2021-02-10_12.11.32", 
"strip_2021-02-10_12.13.30", 
"strip_2021-02-10_12.13.42", 
"strip_2021-02-10_18.20.28"]
'''


edited_pearls_folder = "edited_pearls/"


for strip_base in strip_bases:


	original_txt = "{}{}_update.txt".format(edited_pearls_folder, strip_base)

	with open(original_txt) as f:
		original_contents = f.read()

	#print(original_contents)

	pattern = r"\d\d\d\d-\d\d-\d\d"
	speech_dates = re.findall(pattern, original_contents)
	print(speech_dates)


	#gör en ny uppdatering

	with open("hashtags.txt") as f:
		hashtags = f.readlines()

	hashtags = [x.strip() for x in hashtags] #strip newlines
	hashtags_chosen = random.sample(hashtags, 14) #choose N unique hashtags
	random.shuffle(hashtags_chosen) #randomise order of hashtags

	update_part1 = random.sample(["Skål!", "Hejsan!", "Puss och kram!", "Medborgare!", "Kansalaiset!", "Hear, hear!", "The smörgåsbord is set!", "You are the dancing queen!", "This is the happiest hello in the world!", "Moi!", "Moi!", "Moi.", "Moi!", "Moi!", "Moi!", "Terve!", "Terve.", "Terve!", "Terve!", "Long live Nordic cooperation!", "Greetings from the most peaceful corner of the world!", "And the discussion still goes on. These guys never get tired of talking!", "Hot political topics from the lands of reindeer, snow and midnight sun.", "In a parallel universe, this is what the Nordic leaders talk about.", "Jaw-dropping clarity!", "Let's have a toast to our cherished Nordic values!", "Gotcha! I think ...", "Well said, bravo!", "What would their viking ancestors say?", "The diplomatic ties across the Baltic sea are strong!", "", "", "", "", ""], 1)

	update_part2 = random.sample(["This fictional discussion between the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven, is based on a comic strip algorithm created by @sebastian.dahlstrom.drawings and @finnowlgram. These words of wisdom were picked from speeches held on {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Nordic Diplomacy is created by @sebastian.dahlstrom.drawings and @finnowlgram. The algorithm combines quotes from speeches by the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven. These speeches were held on {}, {}, {}, {}, {} and {} respectively.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Created by @sebastian.dahlstrom.drawings and @finnowlgram. For this strip, our algorithm dissected speeches by the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. These quotes are from {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "This fictional conversation between the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö, is based on speeches held on {}, {}, {}, {}, {} and {}. A comic strip algorithm created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Nordic Diplomacy is created by @sebastian.dahlstrom.drawings and @finnowlgram. With our algorithm that picks out quotes from speeches, we got this fictional dialogue between our Nordic leaders Mr. Sauli Niinistö and Mr. Stefan Löfven (the President of Finland vs. the Prime Minister of Sweden). The quotes are from {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Crystal clear, like the water the Nordic leader drank from the mountain stream. This fictional dialogue between the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven, is based on speeches held on {}, {}, {}, {}, {} and {}. An algorithm-based comic strip created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "This fictional dialogue was created from speeches held on {}, {}, {}, {}, {} and {} by the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven. An algorithm-based comic strip created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "This fictional conversation is based on speeches held on {}, {}, {}, {}, {} and {}. Created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "What is subtler than subtlest? Nordic Diplomacy. A fictional discussion between the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. The speeches quoted here were held on {}, {}, {}, {}, {} and {}. Created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "A comic strip starring the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. For this strip, our algorithm dissected speeches held on {}, {}, {}, {}, {} and {}. Created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "A fictional conversation between the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö, based on speeches held on {}, {}, {}, {}, {} and {}. A comic strip algorithm created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "With out algorithm that picks quotes from real speeches, we got this fictional conversation between our Nordic leaders the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. Nordic Diplomacy is created by @sebastian.dahlstrom.drawings and @finnowlgram. Speeches held on {}, {}, {}, {}, {} and {} respectively.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "A fictional tête-à-tête between the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven. The dialogue is based on excerpts from speeches held on {}, {}, {}, {}, {} and {}. An algorithm-based comic strip created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5])], 1)

	update_part25 = random.sample(["Curious about the algorithm? Check out the link in bio.", "Curious about the algorithm? Check out the link in bio.", "Curious about the algorithm? Check out the link in bio.", "Curious about the algorithm? Check out the link in bio.", "How was the algorithm created? Check out the link in bio.", "Check out the link in bio for some background on the algorithm."], 1)

	#add standard hashtags and scrable
	hashtags_chosen = hashtags_chosen + ["#sauliniinistö", "#stefanlöfven", "#nordicdiplomacy", "#nordiccollaboration", "#nordicleaders", "#nordicpolitics"]
	random.shuffle(hashtags_chosen)

	update_part3 = " ".join(hashtags_chosen)

	emojis = [["😍", "(in love)"], ["😍", "(in love)"], ["😍", "(in love)"], ["😍", "(in love)"], ["☝️", "(index finger)"], ["☝️", "(index finger)"], ["☝️", "(index finger)"], ["🤗", "(applause)"], ["🤯", "(angry)"], ["🤭", "(ashamed)"], ["👋", "(waving)"], ["🌈", "(rainbow)"], ["🦄", "(unicorn)"], ["🕊", "(dove)"], ["💎", "(diamond)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["😅", "(sweating laugh)"], ["😅", "(sweating laugh)"], ["😅", "(sweating laugh)"], ["🤓", "(smiley glasses)"], ["🤓", "(smiley glasses)"]]

	emoji_rand = random.randint(0,len(emojis)-1)


	new_update_text = "{} {} {}\n{} 🇫🇮 🇸🇪\n{}".format(update_part1[0], emojis[emoji_rand][0], update_part2[0], update_part25[0], update_part3)

	new_update_text = new_update_text.lstrip()

	print(new_update_text)


	update_text_file = "{}{}_update_new_new.txt".format(edited_pearls_folder, strip_base)

	with open(update_text_file, "w") as text_file:
		text_file.write(new_update_text)

