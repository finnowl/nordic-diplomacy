from os import listdir
import os
from os.path import isfile, join
import pprint
import sys
import shutil
from PIL import Image, ImageDraw, ImageFont, ImageTk


accepted_path = "accepted_strips/"
stamped_path = "stamped_strips/"
file_name = "file_listing.txt"

font_path = "/Library/Fonts/Courier New.ttf"
font_size = 22


#open file_listing as list

strip_order = [line.rstrip('\n') for line in open("{}{}".format(accepted_path, file_name))]
pprint.pprint(strip_order)



#get all relevant files from accepted folder

only_files = [f for f in listdir(accepted_path) if isfile(join(accepted_path, f))] #only files, no directories if any

relevant_files = [] #png and txt files

for x in only_files: #clean irrelevant

	if ".png" in x or (".txt" in x and x != file_name):
		relevant_files.append(x)

#pprint.pprint(relevant_files)


#get input for starting number

starting_number = input("\nWhat number should the stamping start from? ")

if starting_number.isnumeric() == False:
	print("Incorrect input, not a number.")
	sys.exit(0)
elif int(starting_number) == 0:
	print("Incorrect input, should be more than 0.")
	sys.exit(0)

question = input("\nShould numbering start from {}? (y) ".format(starting_number))

if question.lower() != ("y"):
	print("Script exited.")
	sys.exit(0)


#open grafic files one by one, in order as in strip_order

number = int(starting_number)

for x in strip_order:

	endings = ["1.png", "2.png", "3.png", "full.png", "update.txt"]

	for y in endings:

		copy_from_path = "{}{}_{}".format(accepted_path, x, y)

		copy_to_path = "{}{}_{}_{}".format(stamped_path, number, x, y)

		#HÄR LÄGGA TILL NUMRET I GRAFIKEN DÄR DET BEHÖVS

		if "full.png" in y or "3.png" in y: #for full and 3 image files

			if "3.png" in y:
				x_coord = 1020
				y_coord = 910
			if "full.png" in y:
				x_coord = 3180
				y_coord = 910

			img_file = Image.open(copy_from_path, "r") #open existing image

			text_image = Image.new("RGB", (100, 100), (255, 255, 255))

			font = ImageFont.truetype(font_path, font_size)

			pic_with_text = ImageDraw.Draw(text_image)

			pic_with_text.text((0, 0), str(number), font=font, fill=(0, 0, 0))

			text_image = text_image

			img_file.paste(text_image.rotate(90), (x_coord,y_coord))

			img_file.save(copy_to_path, format="png")

		else: #for 1 and 2 image files and txt file
			copied_path = shutil.copy(copy_from_path, copy_to_path)

		#print(copy_from_path)
		#print(copy_to_path)

	number = number + 1










