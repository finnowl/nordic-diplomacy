# Algorithm-based comic strip bot

In March-June 2021, a automatically generated comic strip was published every day on Instagram (https://www.instagram.com/nordicdiplomacy/).

The techniques and libraries used are decribed in detail in this [blog posting](https://www.sebastiandahlstrom.com/blog/can-an-algorithm-be-funny-this-is-how-we-created-an-automatically-generated).

**nordic-diplomacy.py** contains the bot itself, including a GUI specifically created for MacOS. The other scripts are for ingesting quotes into the SQLite database as well as organizing the generated comic strips in different ways.
