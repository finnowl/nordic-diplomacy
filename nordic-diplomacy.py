###IMPORTS

import random
from time import sleep
from datetime import datetime
import os
import sqlite3
import shutil

from hyphen import Hyphenator
from textwrap2 import fill
import dropbox
from tkinter import *
import tkinter as tk
from tkmacosx import Button
from PIL import Image, ImageDraw, ImageFont, ImageTk


###SET OPTIONS

print_results = True
upload_to_dropbox = False
local_folder = "generated_strips/"
accepted_folder = "accepted_strips/"
template_folder = "templates_2021-01-23/"
h_maxwidth = 17 #max length of lines
font_path = "/Library/Fonts/Courier New.ttf"
font_size = 38
renew_last_line = False



###CREATE GLOBAL VARIABLES

#filenames = []
update_text = ""
access_token = "" #Dropbox

result1 = None
result2 = None
result3 = None
result4 = None
result5 = None
result5 = None

replik1 = None
replik2 = None
replik3 = None
replik4 = None
replik5 = None
replik6 = None

odd_speaker = None
odd_name = None
even_speaker = None
even_name = None

img_box = None
created_box = None
discard_button = None
accept_button = None
status_box = None
check_var1 = None
check_var2 = None

strip_data = {
	"setting": None,
	"title" : None,
	"foreground": None, #true or false
	"background": None, #true or false
	"leave_empty": None,
	"leave_empty_line": None,
	"one": {
	"square": None,
	"handle": None,
	"dash_left": None,
	"dash_right": None,
	"left_line": None,
	"right_line": None,
	"left_line_x": None,
	"left_line_y": None,
	"right_line_x": None,
	"right_line_y": None,
	"left_head": None,
	"left_body": None,
	"right_head": None,
	"right_body": None,
	"numbering": None,
	"foreground": None,
	"background": None
	},
	"two": {
	"square": None,
	"handle": None,
	"dash_left": None,
	"dash_right": None,
	"left_line": None,
	"right_line": None,
	"left_line_x": None,
	"left_line_y": None,
	"right_line_x": None,
	"right_line_y": None,
	"left_head": None,
	"left_body": None,
	"right_head": None,
	"right_body": None,
	"numbering": None,
	"foreground": None,
	"background": None
	},
	"three": {
	"square": None,
	"handle": None,
	"dash_left": None,
	"dash_right": None,
	"left_line": None,
	"right_line": None,
	"left_line_x": None,
	"left_line_y": None,
	"right_line_x": None,
	"right_line_y": None,
	"left_head": None,
	"left_body": None,
	"right_head": None,
	"right_body": None,
	"numbering": None,
	"foreground": None,
	"background": None
	},
	"speech_dates": None,
	"update_text": None,
	"update_text_gui": None
}


def probability(*args):

	rand = random.randint(1,100)

	if len(args) == 0 or len(args) == 1: #fallback if zero or one argument
		return None

	else: #two arguments or more, sum needs to be 100

		if sum(args) != 100:
			print("Probability argument sum not 100.")
			return None

		for x in args:
			if x < 1 or x > 100:
				print("Probability arguments out or range.")
				return None

		y = 1
		returns = []

		for x in args:

			if rand >= y and rand <= y+x-1:
				returns.append(True)
			else:
				returns.append(False)

			y = y + x #for next round

		return returns #a list


def get_related(quote_obj, where_string):

	#get relevant word ids for quote
	li_sql = """SELECT * FROM cross_ref WHERE quote_id='{}';""".format(quote_obj["quote_id"])
	cur.execute(li_sql)
	words = cur.fetchall()
	rel_words = [x["word_id"] for x in words]
	#print("Rel words: {}".format(rel_words))

	#get related quotes for the word ids
	rel_quotes = []

	for x in rel_words: #rel_words is a list of word ids

		li_sql = """SELECT * FROM cross_ref WHERE word_id='{}';""".format(x)
		cur.execute(li_sql)
		rel_quotes_list = cur.fetchall()

		rel_quotes_list = list(dict.fromkeys(rel_quotes_list)) #remove duplicates

		for y in rel_quotes_list: #returns a tuple (quote_id, word_id)

			li_sql = """SELECT * FROM quotes {} AND quote_id='{}';""".format(where_string, y[0])
			cur.execute(li_sql)
			result = cur.fetchone()

			if result == None: #if no match
				pass
			else:
				rel_quotes.append([result["quote_id"], result["sentence"], result["speech_id"]])

	#remove original quote from related quotes
	rel_quotes_cleaned = []

	for idx, val in enumerate(rel_quotes):
		if val[1] != quote_obj["sentence"]:
			rel_quotes_cleaned.append(val)


	quote_type = ""

	if not rel_quotes_cleaned: #if empty list = no matches, so get random quote instead
		quote_type = "Unrelated"

		li_sql = """SELECT * FROM quotes {};""".format(where_string)
		cur.execute(li_sql)
		result = cur.fetchone()

		rel_quote_chosen = [result["quote_id"], result["sentence"], result["speech_id"]] #list

	else: #if list contains matches
		quote_type = "Related"
		rel_quote_chosen = random.choice(rel_quotes_cleaned) #randomly choose one, returns list item
	

	return_dict = {"relation": quote_type, "sentence": rel_quote_chosen[1], "quote_id": rel_quote_chosen[0], "speech_id": rel_quote_chosen[2]}

	return return_dict



def accept(z = None):

	status_box.insert(END, "Uploading/saving ...\n")
	status_box.see("end")

	root.after(500, save_upload_dropbox)
	root.after(2000, run_bot)
	root.after(8000, accept_update)


def accept_update(z = None):

	img_box_load = Image.open(filenames_strip)
	img_box_load_resized = img_box_load.resize((1296, 432))
	img_box_object = ImageTk.PhotoImage(img_box_load_resized)
	img_box.configure(image=img_box_object)
	img_box.image = img_box_object
	img_box.grid(row = 0, column=0, rowspan = 1 , stick = "w")

	created_box.config(text = filenames_strip)

	update_box.config(text = strip_data["update_text_gui"])


def save_upload_dropbox(z = None):

	global check_var1
	global check_var2

	if check_var1.get() == 1:

		for x in filenames:

			copy_to_path = "{}{}".format(accepted_folder, x.replace(local_folder, ""))
			copied_path = shutil.copy(x, copy_to_path)
			print("      Copied {} to {}".format(x, copied_path))

		status_box.insert(END, "Files saved locally ({})\n".format(filenames[3][:-9])) #remove "_full.png" from end

	if check_var2.get() == 1:

		print("   Connecting to Dropbox.")

		for x in filenames:
			file_from = x
			file_to = "/Caricature-bot/{}".format(x.replace(local_folder, ""))

			dbx = dropbox.Dropbox(access_token)

			with open(file_from, "rb") as f:
				dbx.files_upload(f.read(), file_to)

			print("      Uploaded {} ({} bytes).".format(x.replace(local_folder, ""), os.path.getsize(x)))

		#status_box.delete("1.0","end")

		status_box.insert(END, "Files uploaded to Dropbox ({})\n".format(filenames[3][:-9])) #remove "_full.png" from end
		status_box.see("end")

	if check_var1.get() == 0 and check_var2.get() == 0:

		status_box.insert(END, "NB! Nothing saved or uploaded.\n")
		status_box.see("end")



def discard(z = None):
	status_box.delete("1.0","end")
	status_box.insert(END, "Creating new comic strip ...\n")
	status_box.see("end")

	root.after(2000, run_bot)
	root.after(8000, discard_update)


def discard_update(z = None):

	img_box_load = Image.open(filenames_strip)
	img_box_load_resized = img_box_load.resize((1296, 432))
	img_box_object = ImageTk.PhotoImage(img_box_load_resized)
	img_box.configure(image=img_box_object)
	img_box.image = img_box_object
	img_box.grid(row = 0, column=0, rowspan = 1 , stick = "w")

	created_box.config(text = filenames_strip)

	update_box.config(text = strip_data["update_text_gui"])

	status_box.delete("1.0","end")


def renew(z = None):

	global renew_last_line
	renew_last_line = True

	status_box.delete("1.0","end")
	status_box.insert(END, "Renewing last line of strip ...\n")
	status_box.see("end")

	root.after(2000, run_bot)
	root.after(8000, discard_update)


def run_bot():

	###RANDOMISE ORDER OF SPEAKERS

	global renew_last_line

	if renew_last_line == False:

		global odd_speaker
		global odd_name
		global even_speaker
		global even_name


		odd_speaker = ""
		odd_name = ""
		even_speaker = ""
		even_name = ""

		speaker_rand = probability(50, 50)

		if speaker_rand[0]:
			odd_speaker = "N" #for database
			odd_name = "NIINISTÖ:" #for printing
			even_speaker = "L"
			even_name = "LÖFVEN:"

		elif speaker_rand[1]:
			even_speaker = "N"
			even_name = "NIINISTÖ: "
			odd_speaker = "L"
			odd_name = "LÖFVEN:   "



	###GET DATE AND TIME FOR FILE NAMES

	now = datetime.now()
	now_string = now.strftime("%Y-%m-%d_%H.%M.%S")

	global filenames_strip
	filenames_strip = "{}strip_{}_full.png".format(local_folder, now_string)
	filenames_update_text =  "{}strip_{}_update.txt".format(local_folder, now_string)



	###OPEN DATABASE CONNECTION

	conn = sqlite3.connect("speeches.db")
	conn.row_factory = sqlite3.Row #to be able to access results like dictionary
	global cur
	cur = conn.cursor()


	if renew_last_line == False:

		###1

		global result1
		global replik1

		replik1 = ""

		#long enough (>25) and does not start with And/Or/But

		li_sql = """SELECT * FROM quotes WHERE speaker='{}' AND length<80 AND length>25 AND sentence NOT LIKE 'And%' AND sentence NOT LIKE 'Or%' AND sentence NOT LIKE 'But%' ORDER BY RANDOM() LIMIT 1;""".format(odd_speaker)
		cur.execute(li_sql)

		result1 = cur.fetchone()

		replik1 = result1["sentence"]



		###2

		global result2
		global replik2

		replik2 = ""

		#not a question

		#li_sql = """SELECT * FROM quotes WHERE speaker='{}' AND length<80 AND NOT sentence_type='question' ORDER BY RANDOM() LIMIT 1;""".format(even_speaker)
		#cur.execute(li_sql)
		#result2 = cur.fetchone()

		new_quote = get_related(result1, "WHERE speaker='{}' AND length<80 AND NOT sentence_type='question'".format(even_speaker))

		#print("{} sentence:   {} ({})\n".format(new_quote["relation"], new_quote["sentence"], new_quote["quote_id"]))



		result2 = new_quote
		replik2 = new_quote["sentence"]



		###3

		global result3
		global replik3

		replik3 = ""

		#not Thank you and not like no 1

		#li_sql = """SELECT * FROM quotes WHERE speaker='{}' AND length<60 AND NOT sentence='Thank you.' AND NOT sentence ='{}' ORDER BY RANDOM() LIMIT 1;""".format(odd_speaker, replik1)
		#cur.execute(li_sql)
		#result3 = cur.fetchone()
		#replik3 = result3["sentence"]

		new_quote = get_related(result2, "WHERE speaker='{}' AND length<60 AND NOT sentence='Thank you.' AND NOT sentence ='{}'".format(odd_speaker, replik1))



		result3 = new_quote
		replik3 = new_quote["sentence"]



		###4

		global result4
		global replik4

		replik4 = ""

		#a bit shorter (<35), not same as no 2 and not a question

		#li_sql = """SELECT * FROM quotes WHERE speaker='{}' AND length<35 AND NOT sentence_type='question' AND NOT sentence='{}' ORDER BY RANDOM() LIMIT 1;""".format(even_speaker, replik2)
		#cur.execute(li_sql)
		#result4 = cur.fetchone()

		#replik4 = result4["sentence"]

		new_quote = get_related(result3, "WHERE speaker='{}' AND length<50 AND NOT sentence_type='question' AND NOT sentence='{}'".format(even_speaker, replik2))



		result4 = new_quote
		replik4 = new_quote["sentence"]



		###5

		global result5
		global replik5

		replik5 = ""

		#not Thank you and not like no 1 and no 3

		#li_sql = """SELECT * FROM quotes WHERE speaker='{}' AND length<60 AND NOT sentence='Thank you.' AND NOT sentence ='{}' AND NOT sentence ='{}' ORDER BY RANDOM() LIMIT 1;""".format(odd_speaker, replik1, replik3)
		#cur.execute(li_sql)
		#result5 = cur.fetchone()

		#replik5 = result5["sentence"]

		new_quote = get_related(result4, "WHERE speaker='{}' AND length<80 AND NOT sentence='Thank you.' AND NOT sentence ='{}' AND NOT sentence ='{}'".format(odd_speaker, replik1, replik3))


		result5 = new_quote
		replik5 = new_quote["sentence"]


	###6


	global result6
	global replik6

	replik6 = ""

	#not a question and not like no 2 and no 4
	#li_sql = """SELECT * FROM quotes WHERE speaker='{}' AND length<60 AND NOT sentence_type='question' AND NOT sentence='{}' AND NOT sentence='{}' AND (polarity >= 0.3 OR polarity <= -0.3) ORDER BY RANDOM() LIMIT 1;""".format(even_speaker, replik2, replik4)
	#cur.execute(li_sql)
	#result6 = cur.fetchone()

	#replik6 = result6["sentence"]

	new_quote = get_related(result5, "WHERE speaker='{}' AND length<60 AND NOT sentence='{}' AND NOT sentence='{}'".format(even_speaker, replik2, replik4))

	result6 = new_quote
	replik6 = new_quote["sentence"]



	###GET DATES FOR SPEECHES

	speech_dates = []
	speech_ids = [result1["speech_id"], result2["speech_id"], result3["speech_id"], result4["speech_id"], result5["speech_id"], result6["speech_id"]]

	for x in speech_ids:
		li_sql = """SELECT * FROM speeches WHERE speech_id={};""".format(x)
		cur.execute(li_sql)
		result = cur.fetchone()
		speech_dates.append(result["date"])

	strip_data["speech_dates"] = speech_dates 



	###CLOSE DATABASE CONNECTION

	cur.close()
	conn.close()



	###HYPHENATE

	h_en = Hyphenator("en_US")

	replik1_hyp = fill(replik1, width=h_maxwidth, use_hyphenator=h_en).split("\n") #split into list based on newline
	replik2_hyp = fill(replik2, width=h_maxwidth, use_hyphenator=h_en).split("\n") #split into list based on newline
	replik3_hyp = fill(replik3, width=h_maxwidth, use_hyphenator=h_en).split("\n") #split into list based on newline
	replik4_hyp = fill(replik4, width=h_maxwidth, use_hyphenator=h_en).split("\n") #split into list based on newline
	replik5_hyp = fill(replik5, width=h_maxwidth, use_hyphenator=h_en).split("\n") #split into list based on newline
	replik6_hyp = fill(replik6, width=h_maxwidth, use_hyphenator=h_en).split("\n") #split into list based on newline

	strip_data["one"]["left_line"] = replik1_hyp
	strip_data["one"]["right_line"] = replik2_hyp
	strip_data["two"]["left_line"] = replik3_hyp
	strip_data["two"]["right_line"] = replik4_hyp
	strip_data["three"]["left_line"] = replik5_hyp
	strip_data["three"]["right_line"] = replik6_hyp



	###PRINT RESULTS

	print("\n")

	if print_results:
		print("{:10} {} \n          {}".format(odd_name, replik1, replik1_hyp))
		print("{:10} {} \n          {}".format(even_name, replik2, replik2_hyp))
		print("{:10} {} \n          {}".format(odd_name, replik3, replik3_hyp))
		print("{:10} {} \n          {}".format(even_name, replik4, replik4_hyp))
		print("{:10} {} \n          {}".format(odd_name, replik5, replik5_hyp))
		print("{:10} {} \n          {}".format(even_name, replik6, replik6_hyp))
		print("Speeches held on {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]))
		print("\n")

	print("   Quotes chosen and processed.")



	###RANDOMIZE GRAPHICS

	if renew_last_line == False:

		#square
		square_rand = random.sample(range(1,6), 3)
		strip_data["one"]["square"] = "square{}.png".format(square_rand[0])
		strip_data["two"]["square"] = "square{}.png".format(square_rand[1])
		strip_data["three"]["square"] = "square{}.png".format(square_rand[2])

		#handle ("@nordicdiplomacy")
		handle_rand = random.sample(range(1,6), 3)
		strip_data["one"]["handle"] = "handle{}.png".format(handle_rand[0])
		strip_data["two"]["handle"] = "handle{}.png".format(handle_rand[1])
		strip_data["three"]["handle"] = "handle{}.png".format(handle_rand[2])

		#numbering
		strip_data["one"]["numbering"] =    "Numbering_square1_v{}.png".format(random.randint(1,2))
		strip_data["two"]["numbering"] =    "Numbering_square2_v{}.png".format(random.randint(1,2))
		strip_data["three"]["numbering"] =  "Numbering_square3_v{}.png".format(random.randint(1,2))



		#setting
		#setting_list = ["setting1.png", "setting2.png", "setting3.png", "setting4.png"]
		#strip_data["setting"] = random.choice(setting_list)


		#heads
		head_rand1 = random.sample(range(1,13), 3)
		strip_data["one"]["left_head"] =    "{}_N_head{}.png".format(odd_speaker, head_rand1[0])
		strip_data["two"]["left_head"] =    "{}_N_head{}.png".format(odd_speaker, head_rand1[1])
		strip_data["three"]["left_head"] =  "{}_N_head{}.png".format(odd_speaker, head_rand1[2])

		head_rand2 = random.sample(range(1,13), 3)
		strip_data["one"]["right_head"] =   "{}_L_head{}.png".format(odd_speaker, head_rand2[0])
		strip_data["two"]["right_head"] =   "{}_L_head{}.png".format(odd_speaker, head_rand2[1])
		strip_data["three"]["right_head"] = "{}_L_head{}.png".format(odd_speaker, head_rand2[2])


		#bodies in three categories
		body_type_rand = probability(10, 5, 85)

		if body_type_rand[0]: #informal bodies
			body_rand1 = random.sample(range(1,6), 3)
			strip_data["one"]["left_body"] =    "{}_N_body_informal{}.png".format(odd_speaker, body_rand1[0])
			strip_data["two"]["left_body"] =    "{}_N_body_informal{}.png".format(odd_speaker, body_rand1[1])
			strip_data["three"]["left_body"] =  "{}_N_body_informal{}.png".format(odd_speaker, body_rand1[2])

			body_rand2 = random.sample(range(1,6), 3)
			strip_data["one"]["right_body"] =   "{}_L_body_informal{}.png".format(odd_speaker, body_rand2[0])
			strip_data["two"]["right_body"] =   "{}_L_body_informal{}.png".format(odd_speaker, body_rand2[1])
			strip_data["three"]["right_body"] = "{}_L_body_informal{}.png".format(odd_speaker, body_rand2[2])

			foreground_rand = probability(10, 90)
			if foreground_rand[0]:
				strip_data["foreground"] = True
				foregound_chosen_rand = random.choice(["Foreground_outdoor_fence.png", "Foreground_podium.png", "Foreground_outdoor_flowers.png"])
				strip_data["one"]["foreground"] = foregound_chosen_rand
				strip_data["two"]["foreground"] = foregound_chosen_rand
				strip_data["three"]["foreground"] = foregound_chosen_rand

			elif foreground_rand[1]:
				strip_data["foreground"] = False #reset for next run

			background_rand = probability(10, 90)
			if background_rand[0]:
				strip_data["background"] = True
				background_chosen_rand = random.choice([["Background_outdoor_cloud_series1_1.png", "Background_outdoor_cloud_series1_2.png", "Background_outdoor_cloud_series1_3.png"], ["Background_outdoor_cloud_series2_1.png", "Background_outdoor_cloud_series2_2.png", "Background_outdoor_cloud_series2_3.png"]])
				strip_data["one"]["background"] = background_chosen_rand[0]
				strip_data["two"]["background"] = background_chosen_rand[1]
				strip_data["three"]["background"] = background_chosen_rand[2]

			elif background_rand[1]:
				strip_data["background"] = False #reset for next run


		elif body_type_rand[1]: #sauna bodies and sauna foreground+background 5 % chance
			body_rand1 = random.sample(range(1,5), 3)
			strip_data["one"]["left_body"] =    "{}_N_body_sauna{}.png".format(odd_speaker, body_rand1[0])
			strip_data["two"]["left_body"] =    "{}_N_body_sauna{}.png".format(odd_speaker, body_rand1[1])
			strip_data["three"]["left_body"] =  "{}_N_body_sauna{}.png".format(odd_speaker, body_rand1[2])

			body_rand2 = random.sample(range(1,5), 3)
			strip_data["one"]["right_body"] =   "{}_L_body_sauna{}.png".format(odd_speaker, body_rand2[0])
			strip_data["two"]["right_body"] =   "{}_L_body_sauna{}.png".format(odd_speaker, body_rand2[1])
			strip_data["three"]["right_body"] = "{}_L_body_sauna{}.png".format(odd_speaker, body_rand2[2])

			strip_data["foreground"] = True
			strip_data["background"] = True

			strip_data["one"]["foreground"] = "foreground_sauna.png"
			strip_data["two"]["foreground"] = "foreground_sauna.png"
			strip_data["three"]["foreground"] = "foreground_sauna.png"			
			strip_data["one"]["background"] = "background_sauna.png"
			strip_data["two"]["background"] = "background_sauna.png"
			strip_data["three"]["background"] = "background_sauna.png"


		elif body_type_rand[2]: #formal bodies 85 % chance
			body_rand1 = random.sample(range(1,7), 3)
			strip_data["one"]["left_body"] =    "{}_N_body{}.png".format(odd_speaker, body_rand1[0])
			strip_data["two"]["left_body"] =    "{}_N_body{}.png".format(odd_speaker, body_rand1[1])
			strip_data["three"]["left_body"] =  "{}_N_body{}.png".format(odd_speaker, body_rand1[2])
		
			body_rand2 = random.sample(range(1,7), 3)
			strip_data["one"]["right_body"] =   "{}_L_body{}.png".format(odd_speaker, body_rand2[0])
			strip_data["two"]["right_body"] =   "{}_L_body{}.png".format(odd_speaker, body_rand2[1])
			strip_data["three"]["right_body"] = "{}_L_body{}.png".format(odd_speaker, body_rand2[2])



			foreground_rand = probability(10, 90)
			if foreground_rand[0]:
				strip_data["foreground"] = True
				foregound_chosen_rand = random.choice(["Foreground_outdoor_fence.png", "Foreground_podium.png", "Foreground_outdoor_flowers.png", "Foreground_outdoor_car.png"])
				strip_data["one"]["foreground"] = foregound_chosen_rand
				strip_data["two"]["foreground"] = foregound_chosen_rand
				strip_data["three"]["foreground"] = foregound_chosen_rand
			elif foreground_rand[1]:
				strip_data["foreground"] = False #reset for next run

			#special case for car foreground that requires car background
			if strip_data["one"]["foreground"] == "Foreground_outdoor_car.png":

				strip_data["one"]["background"] = "Background_outdoor_car.png"
				strip_data["two"]["background"] = "Background_outdoor_car.png"
				strip_data["three"]["background"] = "Background_outdoor_car.png"

			else: #if not car foreground

				background_rand = probability(10, 90)
				if background_rand[0]:
					strip_data["background"] = True
					background_chosen_rand = random.choice([["Background_outdoor_cloud_series1_1.png", "Background_outdoor_cloud_series1_2.png", "Background_outdoor_cloud_series1_3.png"], ["Background_outdoor_cloud_series2_1.png", "Background_outdoor_cloud_series2_2.png", "Background_outdoor_cloud_series2_3.png"]])
					strip_data["one"]["background"] = background_chosen_rand[0]
					strip_data["two"]["background"] = background_chosen_rand[1]
					strip_data["three"]["background"] = background_chosen_rand[2]

				elif background_rand[1]:
					strip_data["background"] = False #reset for next run


		
		#leave one line empty and replace with sign
		leave_empty_rand = probability(15, 85) 

		if leave_empty_rand[0]: # 15 % chance

			strip_data["leave_empty_line"] = random.choice([4, 6]) #line 4 och line 6

			replace_signs = ["sign_dizzy.png", "sign_frustrated.png", "sign_headache.png", "sign_question.png", "sign_surprised.png", "sign_waiting.png", "sign_question.png", "sign_question.png", "sign_surprised.png"] #which sign to show

			strip_data["leave_empty"] = random.choice(replace_signs)

		elif leave_empty_rand[1]: #reset variables
			strip_data["leave_empty"] = None 
			strip_data["leave_empty_line"] = None


		#title
		title_rand = random.sample(range(1,4), 1)
		strip_data["title"] = "title{}.png".format(title_rand[0])



	###SET COORDINATES FOR SPEECH BUBBLES

	length_list = [replik1_hyp, replik2_hyp, replik3_hyp, replik4_hyp, replik5_hyp, replik6_hyp]
	length_list_coord = []

	left_line_x_standard = 125
	right_line_x_standard = 570

	for x in length_list:
		length_list_coord.append(385 - (len(x) * 37))

	strip_data["one"]["left_line_x"] = left_line_x_standard
	strip_data["one"]["left_line_y"] = length_list_coord[0] + 37
	strip_data["one"]["right_line_x"] = right_line_x_standard
	strip_data["one"]["right_line_y"] = length_list_coord[1] + 37

	strip_data["two"]["left_line_x"] = left_line_x_standard
	strip_data["two"]["left_line_y"] = length_list_coord[2] - 37
	strip_data["two"]["right_line_x"] = right_line_x_standard
	strip_data["two"]["right_line_y"] = length_list_coord[3] - 37

	strip_data["three"]["left_line_x"] = left_line_x_standard
	strip_data["three"]["left_line_y"] = length_list_coord[4] - 37
	strip_data["three"]["right_line_x"] = right_line_x_standard
	strip_data["three"]["right_line_y"] = length_list_coord[5] - 37

	global filenames
	filenames = []


	###COMBINE GRAPHICAL ELEMENTS

	for x in [["one", 1], ["two", 2], ["three", 3]]:

		
		pic_background = Image.new("RGB", (1080, 1080), (255, 255, 255)) #create empty white background

		#background setting
		if strip_data["background"] == True:
			pic_setting_file = "{}{}".format(template_folder, strip_data[x[0]]["background"])
			pic_setting = Image.open(pic_setting_file, "r")
			pic_background.paste(pic_setting, (0,0), mask=pic_setting)

		#bodies
		pic_body1_file = "{}{}".format(template_folder, strip_data[x[0]]["left_body"])
		pic_body1 = Image.open(pic_body1_file, "r")
		pic_background.paste(pic_body1, (0,0), mask=pic_body1)

		pic_body2_file = "{}{}".format(template_folder, strip_data[x[0]]["right_body"])
		pic_body2 = Image.open(pic_body2_file, "r")
		pic_background.paste(pic_body2, (0,0), mask=pic_body2)

		#heads
		pic_head1_file = "{}{}".format(template_folder, strip_data[x[0]]["left_head"])
		pic_head1 = Image.open(pic_head1_file, "r")
		pic_background.paste(pic_head1, (0,0), mask=pic_head1)

		pic_head2_file = "{}{}".format(template_folder, strip_data[x[0]]["right_head"])
		pic_head2 = Image.open(pic_head2_file, "r")
		pic_background.paste(pic_head2, (0,0), mask=pic_head2)

		#square
		pic_square_file = "{}{}".format(template_folder, strip_data[x[0]]["square"])
		pic_square = Image.open(pic_square_file, "r")
		pic_background.paste(pic_square, (0,0), mask=pic_square)

		#handle
		pic_handle_file = "{}{}".format(template_folder, strip_data[x[0]]["handle"])
		pic_handle = Image.open(pic_handle_file, "r")
		pic_background.paste(pic_handle, (0,0), mask=pic_handle)

		#foreground
		if strip_data["foreground"] == True:
			pic_setting_file = "{}{}".format(template_folder, strip_data[x[0]]["foreground"])
			pic_setting = Image.open(pic_setting_file, "r")
			pic_background.paste(pic_setting, (0,0), mask=pic_setting)

		#title only on first square
		if x[1] == 1:
			pic_title_file = "{}{}".format(template_folder, strip_data["title"])
			pic_title = Image.open(pic_title_file, "r")
			pic_background.paste(pic_title, (0,0), mask=pic_title)

		#numbering
		pic_numbering_file = "{}{}".format(template_folder, strip_data[x[0]]["numbering"]) #inserts "one", "two", "three"
		pic_title = Image.open(pic_numbering_file, "r")
		pic_background.paste(pic_title, (0,0), mask=pic_title)

		#texts
		font = ImageFont.truetype(font_path, font_size)
		pic_with_text = ImageDraw.Draw(pic_background)

		pic_with_text.text((strip_data[x[0]]["left_line_x"],strip_data[x[0]]["left_line_y"]), "\n".join(strip_data[x[0]]["left_line"]), font=font, fill=(0, 0, 0))

		#leave line 4 empty
		if strip_data["leave_empty"] != None and strip_data["leave_empty_line"] == 4 and x[1] == 2: #only for square 2
			pic_replace_line_file = "{}{}".format(template_folder, strip_data["leave_empty"])
			pic_title = Image.open(pic_replace_line_file, "r")
			pic_background.paste(pic_title, (0,0), mask=pic_title)

		#leave line 6 empty
		elif strip_data["leave_empty"] != None and strip_data["leave_empty_line"] == 6 and x[1] == 3: #only for square 3
			pic_replace_line_file = "{}{}".format(template_folder, strip_data["leave_empty"])
			pic_title = Image.open(pic_replace_line_file, "r")
			pic_background.paste(pic_title, (0,0), mask=pic_title)

		#write line 4 like usually
		else:
			pic_with_text.text((strip_data[x[0]]["right_line_x"],strip_data[x[0]]["right_line_y"]), "\n".join(strip_data[x[0]]["right_line"]), font=font, fill=(0, 0, 0))




		img_filename =  "{}strip_{}_{}.png".format(local_folder, now_string, x[1]) #create filename

		filenames.append(img_filename)
		pic_background.save(img_filename, format="PNG") #save to local folder




	###COMBINE THREE IMG TO A STRIP

	filenames.append(filenames_strip) #becomes fourth element

	strip_img = Image.new("RGBA", (3240,1080), (0, 0, 0, 0)) #create background

	stripfile1 = Image.open(filenames[0], "r")
	stripfile2 = Image.open(filenames[1], "r")
	stripfile3 = Image.open(filenames[2], "r")

	strip_img.paste(stripfile1, (0,0)) #first
	strip_img.paste(stripfile2, (1080,0)) #second
	strip_img.paste(stripfile3, (2160,0)) #third

	strip_img.save(filenames_strip, format="png")

	print("   Graphics created.")



	###CREATE UPDATE TEXT

	with open("hashtags.txt") as f:
	    hashtags = f.readlines()

	hashtags = [x.strip() for x in hashtags] #strip newlines
	hashtags_chosen = random.sample(hashtags, 14) #choose N unique hashtags
	random.shuffle(hashtags_chosen) #randomise order of hashtags

	update_part1 = random.sample(["Skål!", "Hejsan!", "Puss och kram!", "Medborgare!", "Kansalaiset!", "Hear, hear!", "The smörgåsbord is set!", "You are the dancing queen!", "This is the happiest hello in the world!", "Moi!", "Moi!", "Moi.", "Moi!", "Moi!", "Moi!", "Terve!", "Terve.", "Terve!", "Terve!", "Long live Nordic cooperation!", "Greetings from the most peaceful corner of the world!", "And the discussion still goes on. These guys never get tired of talking!", "Hot political topics from the lands of reindeer, snow and midnight sun.", "In a parallel universe, this is what the Nordic leaders talk about.", "Jaw-dropping clarity!", "Let's have a toast to our cherished Nordic values!", "Gotcha! I think ...", "Well said, bravo!", "What would their viking ancestors say?", "The diplomatic ties across the Baltic sea are strong!", "", "", "", "", ""], 1)

	update_part2 = random.sample(["This fictional discussion between the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven, is based on a comic strip algorithm created by @sebastian.dahlstrom.drawings and @finnowlgram. These words of wisdom were picked from speeches held on {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Nordic Diplomacy is created by @sebastian.dahlstrom.drawings and @finnowlgram. The algorithm combines quotes from speeches by the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven. These speeches were held on {}, {}, {}, {}, {} and {} respectively.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Created by @sebastian.dahlstrom.drawings and @finnowlgram. For this strip, our algorithm dissected speeches by the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. These quotes are from {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "This fictional conversation between the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö, is based on speeches held on {}, {}, {}, {}, {} and {}. A comic strip algorithm created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Nordic Diplomacy is created by @sebastian.dahlstrom.drawings and @finnowlgram. With our algorithm that picks out quotes from speeches, we got this fictional dialogue between our Nordic leaders Mr. Sauli Niinistö and Mr. Stefan Löfven (the President of Finland vs. the Prime Minister of Sweden). The quotes are from {}, {}, {}, {}, {} and {}.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "Crystal clear, like the water the Nordic leader drank from the mountain stream. This fictional dialogue between the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven, is based on speeches held on {}, {}, {}, {}, {} and {}. An algorithm-based comic strip created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "This fictional dialogue was created from speeches held on {}, {}, {}, {}, {} and {} by the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven. An algorithm-based comic strip created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "This fictional conversation is based on speeches held on {}, {}, {}, {}, {} and {}. Created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "What is subtler than subtlest? Nordic Diplomacy. A fictional discussion between the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. The speeches quoted here were held on {}, {}, {}, {}, {} and {}. Created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "A comic strip starring the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. For this strip, our algorithm dissected speeches held on {}, {}, {}, {}, {} and {}. Created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "A fictional conversation between the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö, based on speeches held on {}, {}, {}, {}, {} and {}. A comic strip algorithm created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "With out algorithm that picks quotes from real speeches, we got this fictional conversation between our Nordic leaders the Prime Minister of Sweden, Mr. Stefan Löfven, and the President of Finland, Mr. Sauli Niinistö. Nordic Diplomacy is created by @sebastian.dahlstrom.drawings and @finnowlgram. Speeches held on {}, {}, {}, {}, {} and {} respectively.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5]), "A fictional tête-à-tête between the President of Finland, Mr. Sauli Niinistö, and the Prime Minister of Sweden, Mr. Stefan Löfven. The dialogue is based on excerpts from speeches held on {}, {}, {}, {}, {} and {}. An algorithm-based comic strip created by @sebastian.dahlstrom.drawings and @finnowlgram.".format(speech_dates[0], speech_dates[1], speech_dates[2], speech_dates[3], speech_dates[4], speech_dates[5])], 1)

	update_part25 = random.sample(["Curious about the algorithm? Check out the link in bio.", "Curious about the algorithm? Check out the link in bio.", "Curious about the algorithm? Check out the link in bio.", "Curious about the algorithm? Check out the link in bio.", "How was the algorithm created? Check out the link in bio.", "Check out the link in bio for some background on the algorithm."], 1)

	#add standard hashtags and scrable
	hashtags_chosen = hashtags_chosen + ["#sauliniinistö", "#stefanlöfven", "#nordicdiplomacy", "#nordiccollaboration", "#nordicleaders", "#nordicpolitics"]
	random.shuffle(hashtags_chosen)

	update_part3 = " ".join(hashtags_chosen)

	emojis = [["😍", "(in love)"], ["😍", "(in love)"], ["😍", "(in love)"], ["😍", "(in love)"], ["☝️", "(index finger)"], ["☝️", "(index finger)"], ["☝️", "(index finger)"], ["🤗", "(applause)"], ["🤯", "(angry)"], ["🤭", "(ashamed)"], ["👋", "(waving)"], ["🌈", "(rainbow)"], ["🦄", "(unicorn)"], ["🕊", "(dove)"], ["💎", "(diamond)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["🕊", "(dove)"], ["😅", "(sweating laugh)"], ["😅", "(sweating laugh)"], ["😅", "(sweating laugh)"], ["🤓", "(smiley glasses)"], ["🤓", "(smiley glasses)"]]

	emoji_rand = random.randint(0,len(emojis)-1)


	strip_data["update_text_gui"] = "{} {} {}\n{} (fin flag) (swe flag)\n{}".format(update_part1[0], emojis[emoji_rand][1], update_part2[0], update_part25[0], update_part3)

	strip_data["update_text_gui"] = strip_data["update_text_gui"].lstrip()
	
	strip_data["update_text"] = "{} {} {}\n{} 🇫🇮 🇸🇪\n{}".format(update_part1[0], emojis[emoji_rand][0], update_part2[0], update_part25[0], update_part3)

	strip_data["update_text"] = strip_data["update_text"].lstrip()



	###CREATE TEXT FILE WITH UPDATE TEXT

	with open(filenames_update_text, "w") as text_file:
	    text_file.write(strip_data["update_text"])

	filenames.append(filenames_update_text) #becomes fifth element

	print("   Update text created.")

	renew_last_line = False



if __name__ == "__main__":

	run_bot()

	root = Tk()
	root.title("Caricature Bot")

	#image box
	img_box = Label(root)

	img_box_load = Image.open(filenames_strip)
	img_box_load_resized = img_box_load.resize((1296, 432))
	img_box_object = ImageTk.PhotoImage(img_box_load_resized)
	img_box.configure(image=img_box_object)
	img_box.image = img_box_object
	img_box.grid(row = 0, column=0, columnspan = 4, stick = "w")

	#creation box
	created_box = Label(root)
	created_box.config(text = filenames_strip)
	created_box.grid(row = 1, column = 0, stick = "w", padx=(25, 20))


	#discard button
	discard_button = Button(root, text="Discard", borderless = True, command = discard, bg='red')
	discard_button.grid(row = 2, column = 0, stick = "w", padx=(25, 20), pady=(20, 0))

	#renew button
	renew_button = Button(root, text="Renew last line", borderless = True, command = renew, bg='yellow')
	renew_button.grid(row = 3, column = 0, stick = "w", padx=(25, 10), pady=(20, 0))

	#accept button
	accept_button = Button(root, text="Accept", borderless = True, command = accept, bg='green')
	accept_button.grid(row = 4, column = 0, stick = "w", padx=(25, 10), pady=(20, 0))

	#accept options
	check_var1 = IntVar(value=0)
	check_var2 = IntVar(value=1)

	accept_check1_text = "Save locally ({})".format(accepted_folder)
	accept_check1 = Checkbutton(root, text = accept_check1_text, variable = check_var1, onvalue = 1, offvalue = 0)
	accept_check1.grid(row = 5, column = 0, stick = "w", padx=(25, 10), pady=(20, 0))

	accept_check2 = Checkbutton(root, text = "Upload to Dropbox", variable = check_var2, onvalue = 1, offvalue = 0)
	accept_check2.grid(row = 6, column = 0, stick = "w", padx=(25, 10), pady=(0, 0))

	#update box
	update_box = Label(root)
	update_box.config(text = strip_data["update_text_gui"], justify=LEFT, anchor="w", wraplength=600)
	update_box.grid(row = 2, column = 1, stick = "w", rowspan = 4)

	#status box
	status_box = Text(root, height=5, width=80)
	status_box.grid(row = 8, column = 0, stick = "w", columnspan = 2, padx=(25, 20), pady=(20, 0))

	#bind keys
	root.bind("<Return>", accept)
	root.bind("<d>", discard)

	root.mainloop()


