from os import listdir
import os
from os.path import isfile, join
import pprint

accepted_path = "accepted_strips/"
file_name = "file_listing.txt"

only_files = [f for f in listdir(accepted_path) if isfile(join(accepted_path, f))] #only files, no directories

individual_strips = [] #full strips
relevant_files = [] #png and txt files

for x in only_files: #clean irrelevant
	if "_full" in x:
		individual_strips.append(x.replace("_full.png", ""))

	if ".png" in x or ".txt" in x:
		relevant_files.append(x)

	if file_name in relevant_files:
		relevant_files.remove(file_name)


list_file_path = "{}{}".format(accepted_path, file_name)

if os.path.isfile(list_file_path): #check if exists
	
	Question = input("\nListing file already exists. Replace? (y) ")

	if Question.lower() == ("y"):
		list_file = open(list_file_path,'w')
		for x in individual_strips:
			list_file.write(x)
			list_file.write('\n')
		list_file.close()
		print("Listing file created.")

	else:
		print("No file created.")

else: #if no file exists
	list_file = open(list_file_path,'w')
	for x in individual_strips:
		list_file.write(x)
		list_file.write('\n')
	list_file.close()
	print("Listing file created.")

#pprint.pprint(relevant_files)



